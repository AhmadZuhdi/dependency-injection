<?php namespace Zuhdi\DependencyInjection\Resolver;

/*
 * INamespaceBuilder
 * This file is part of the Neyka Core package.
 * This interface act as a contract to be used for config
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

interface IResolver
{

	public function resolveDependency($key);

	public function resolveClassname($className);

}