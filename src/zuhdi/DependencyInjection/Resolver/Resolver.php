<?php namespace Zuhdi\DependencyInjection\Resolver;

/*
 * This file is part of the Neyka Core package.
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 * (c) Rendi Eka Putra Suherman <rendi.eka.putra@gmail.com>
 * (c) Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Resolver implements IResolver
{
	/**
	 * List of Dependency
	 * @var [type]
	 */
	private $configuration;

	/**
	 * list of aliases
	 * @var array
	 */
	private $aliases;

	/**
	 * Injector Object
	 * @var [type]
	 */
	private $injector;

	/**
	 * Container Object
	 * @var [type]
	 */
	private $container;

	/**
	 * Repository
	 * @var array
	 */
	private $repository = array();

	public function __construct(array $dependencies, array $aliases)
	{
		$this->configuration = $dependencies;
		
		$this->aliases = $aliases;
	}

	public function repository()
	{
		return $this->repository;
	}

	/**
	 * [set description]
	 * @param [type] $key   [description]
	 * @param [type] $value [description]
	 */
	public function set($key, $value)
	{
		if (!isset($this->configuration[$key]))
		{
			$this->configuration[$key] = function()
			{
				return new $value;
			};
		}
	}

	/**
	 * resolve dependencies of given class name
	 * @param  string $key 
	 * @return [type]      
	 */
	public function resolveDependency($key)
	{
		$this->repository = array();
		
		return $this->resolve($this->converter($key));
	}

	/**
	 * resolve dependencies
	 * @param  string  $className 
	 * @param  boolean $parent    
	 * @return array             
	 */
	public function resolve($className, $parent = true)
	{
		if (!empty($this->configuration[$className]))
		{
			$requestClass = $this->configuration[$className];

			foreach($requestClass as $key => $value)
			{
				$this->checker($value);
			}

		} else {

			return array();
		}

		$this->checker($className);

		return $this->repository;
	}

	/**
	 * Resolve given alias to real class name
	 * @param  string $className 
	 * @return string            
	 */
	public function resolveClassname($className)
	{
		return $this->converter($className);
	}

	/**
	 * check if given class have dependencies
	 * @param  string $value 
	 * @return array        
	 */
	protected function checker($value)
	{	
		if(is_string($value) && isset($this->configuration[$value]))
		{	
			foreach($this->configuration[$value] as $key => $_value)
			{
				$this->checker($_value);
			}

			$this->repository[$value] = $this->configuration[$value];
		}	
	}

	/**
	 * convert alias to real class name
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public function converter($key)
	{
		return (array_key_exists($key, $this->aliases)) ? $this->aliases[$key] : $key;
	}
};