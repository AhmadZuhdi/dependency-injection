<?php namespace Zuhdi\DependencyInjection;

/*
 * This file is part of the Neyka Core package.
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 * (c) Rendi Eka Putra Suherman <rendi.eka.putra@gmail.com>
 * (c) Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Dependency
{

	/**
	 * Resolver Object
	 * @var object
	 */
	private $resolver;

	/**
	 * Container Object
	 * @var object
	 */
	private $container;

	/**
	 * Injector Object
	 * @var [type]
	 */
	private $injector;

	public function __construct($resolver, $container, $injector = null)
	{

		$this->resolver = $resolver;

		$this->container = $container;

		$this->injector = $injector;

	}

	/**
	 * get an object from container
	 * 
	 * @param  string $key 
	 * @return object      
	 */
	public function get($key)
	{	
		$fullClass  = $this->resolver->resolveClassname($key);

		if($this->container->get($fullClass) !== null) return $this->container->get($fullClass);
		
		$dependency = $this->resolver->resolveDependency($key);

		if(!empty($dependency))
		{
			$this->injector->installs($dependency);

		} else {
			
			$this->injector->installIndependent($fullClass);
		}

		return $this->container->get($fullClass);
	}

	/**
	 * install a new object to container
	 * 
	 * @param string $key   
	 * @param [type] $value 
	 */
	public function set($key, $value)
	{
		$this->container->set($key, $value);
	}

}