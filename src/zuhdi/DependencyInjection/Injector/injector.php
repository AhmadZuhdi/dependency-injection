<?php namespace Zuhdi\DependencyInjection\Injector;

/*
 * This file is part of the Neyka Core package.
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 * (c) Rendi Eka Putra Suherman <rendi.eka.putra@gmail.com>
 * (c) Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Injector implements IInjector
{

	/**
	 * Container Object
	 * @var object
	 */
	private $container;

	/**
	 * configuration array
	 * @var array
	 */
	private $configuration = array();

	public function __construct($container)
	{
		$this->container = $container;
	}

	/**
	 * install dependency
	 * @param  array  $dependencies 
	 * @return [type]               
	 */
	public function installs(array $dependencies)
	{	
		$this->configuration = $dependencies;
		
		foreach($dependencies as $key => $value)
		{	
			if(is_array($value))
			{
				$this->installDependency($key, $value);

			} else if (is_string($value)) {
				
				$this->installIndependent($value);
			}
		}
	}

	/**
	 * install a new object without dependencies
	 * @param  string $class 
	 * @return [type]        
	 */
	public function installIndependent($class)
	{
		if($this->container->get($class) === null)
		{	
			$this->container->set($class, new $class());
		}
	}

	/**
	 * install a new objecto with dependencies
	 * @param  string $class        
	 * @param  array  $dependencies 
	 * @return [type]               
	 */
	public function installDependency($class, array $dependencies)
	{
		$objDependencies = array();

		foreach ($dependencies as $key => $value)
		{	
			if(is_string($value) && isset($this->configuration[$value]) && $this->container->get($value) === null)
			{
				$depend = $this->generateDependencies($this->configuration[$value]);				

				$ReflecClass = new \ReflectionCLass($value);

				$this->container->set($value, $ReflecClass->newInstanceArgs($depend));

			} else if(is_string($value) && $this->container->get($value) === null) {

				$this->container->set($value, new $value());
			}
		}

		if($this->container->get($class) === null)
		{	
			$depend = $this->generateDependencies($dependencies);

			$ReflecClass = new \ReflectionCLass($class);

			$this->container->set($class, $ReflecClass->newInstanceArgs($depend));
		}
	}

	/**
	 * generate dependencies for creating new object
	 * @param  array  $array
	 * @return array        
	 */
	private function generateDependencies(array $array)
	{
		$finalDependency = array();

		foreach ($array as $key => $value)
		{
			if(is_string($value) && $this->container->get($value) === null)
			{	
				$this->container->set($value, new $value());

				$finalDependency[] = $this->container->get($value);

			} else if (is_string($value) && $this->container->get($value) !== null) {

				$finalDependency[] = $this->container->get($value);
				
			} else if (!is_string($value)){

				$finalDependency[] = $value;
			}
		}

		return $finalDependency;
	}

}