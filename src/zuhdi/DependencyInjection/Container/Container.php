<?php namespace Zuhdi\DependencyInjection\Container;

/*
 * This file is part of the Neyka Core package.
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 * (c) Rendi Eka Putra Suherman <rendi.eka.putra@gmail.com>
 * (c) Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Container implements IContainer
{

	/**
	 * Container for Created Object
	 * @var array
	 */
	private $container = array();

	/**
	 * get Object from container
	 * @param  string $key 
	 * @return object      
	 */
	public function get($key)
	{
		return (isset($this->container[$key])) ? $this->container[$key] : null;
	}

	/**
	 * set a new Object to container
	 * @param string $key   
	 * @param object $value 
	 */
	public function set($key, $value)
	{
		$this->container[$key] = $value;

		return $this;
	}
}