<?php namespace Zuhdi\DependencyInjection\Container;

/*
 * This file is part of the Neyka Core package.
 *
 * (c) Ridwan Abadi <devaby@gmail.com>
 * (c) Rendi Eka Putra Suherman <rendi.eka.putra@gmail.com>
 * (c) Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

interface IContainer
{

	public function get($key);

	public function set($key, $value);

}